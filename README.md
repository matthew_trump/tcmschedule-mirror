# tcmschedule

This is a Django+Dojo app I wrote a couple years back, just for personal use and amusement. 
I'm a big fan of <a href="http://www.tcm.com">Turner Classic Movies</a>, and have long relied on their
website to check their listings. After they made changes to their site, I didn't quite like the new
design so I decided to hack their front-end to make my own user interface, to give me the kind of features
I wanted.

The app automatically downloads the monthly schedule from TCM and screenscrapes it, putting the result in the lcoal database. From that info, a new dynamical browser app can be built. 
One advantage of my app is that one can view the schedules from previous days and months, whereas one can generally
not do that on the TCM site itself (which cuts off after twenty-four hours in the past).

The app worked well at the time (2013), but I stopped updating it when I lost consistent access to 
TCM after cordcutting (would love to get it back without cable!).

As such it probably no longer works, since the screenscaping part depends on the html of TCM's site. Also the Django project format is a bit out of date. But I keep it around for reference of how to do 
this kind of application.

Note that the repo here is a straight copy of the files from my bitbucket account repo <a href="https://bitbucket.org/matthew_trump/tcmschedule">https://bitbucket.org/matthew_trump/tcmschedule</a> which contains the version history as the project was initially built.