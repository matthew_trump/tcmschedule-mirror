# Create your views here.
from django.http import HttpResponse
from django.template import RequestContext, loader
from django.core import serializers
from djangoplex.apps.movies.models import WebPage
from djangoplex.apps.movies.models import Movie
from djangoplex.apps.movies.models import Showing
from djangoplex.apps.movies.models import getChoiceFromRaw
from django.utils import simplejson
from StringIO import StringIO
from django.db.models import Max

import datetime
import time

import urllib
import re

sfmt      = "%Y-%m-%d"
tcmprefix = "http://www.tcm.com/schedule/index.html?tz=est&sdate={0}";
def getMaxScheduleDate():
    smax = Showing.objects.all().aggregate(Max('datetime'))
    dmax = time.strftime(sfmt,smax['datetime__max'].timetuple())
    return dmax

def tcmschedule(request):
    sdate = ''
    tdate = time.strftime(sfmt,time.localtime())
    dmax = getMaxScheduleDate()
    try:
        sdate = request.GET['sdate']
    except Exception:
        sdate = tdate      
    t = loader.get_template("movies/tcmschedule.html")
    return HttpResponse(t.render(RequestContext(request,{'tzone':'','dmax':dmax,'sdate':sdate,'use_site_header':True})))

def tcmfetch(request):
    dmax = getMaxScheduleDate()
    t = loader.get_template("movies/tcmfetch.html")
    return HttpResponse(t.render(RequestContext(request,{'dmax':dmax,'use_site_header':True})))

def tcmdownload(request):
    showings = None
    sdate    = None
    rstatus  = 0
    rmessage = None
    try:
        sdate   = request.GET['sdate']
    except:
        rmessage = "Error: no sdate parameter sent in request"
        rstatus  = -2

    if sdate:
        try:
            tcmurl   = tcmprefix.format(sdate)
            pagetext = urllib.urlopen(tcmurl).read()
            #check to see if format of page is unchanged
            m        = re.search('<a title="view in TCM Movie Database" href="(.*?)">',pagetext)
            if m:
                #wpage   = WebPage.objects.create(url=tcmurl,text=content)
                try:
                    showings = tcmparseshowings(pagetext)
                    rmessage = "Successfully downloaded and parsed schedule from "+tcmurl
                    rstatus  = 1
                except:
                    rstatus  = -4;
                    rmessage = "Error: could not parse movies from page text"
            else:
                rmessage = "Error: could not parse any title from page. Page format may have changed."
                rstatus  = -1
        except Exception as exc:
            rstatus = -3
            rmessag = "Error: could not download and parse movies from file"
            
    robj = {}
    robj['status']  = rstatus
    robj['message'] = rmessage
    if showings:
        robj['showings'] = showings
    
    io = StringIO()
    simplejson.dump(robj, io)
    responsetext = io.getvalue()
    return HttpResponse(responsetext)
    

def parsemovie(text):
    m       = re.search('<div class="genre (.*?)Genre(.*?)silentDate">(.*?)<(.*?)<a title="view in TCM Movie Database" href="(.*?)">(.*?)</a> \(([0-9]{4})\)(.*?)runtimeRating">(.*?)<br/>(.*?)<(.*?)',text,re.DOTALL)
    return m

def parsemovie2(text):
    m2 = re.search('movieDescription">(.*?)<',text,re.DOTALL)
    return m2

def parsemovie3(text):
    m3 = re.search('>Cast</span>: (.*?)\n<',text,re.DOTALL)
    return m3

def parsemovie4(text):
    m4 = re.search('>Dir</span>: (.*?)<',text,re.DOTALL)
    return m4

def titlecase(s):
        return re.sub(r"[A-Za-z]+('[A-Za-z]+)?",
                      lambda mo: mo.group(0)[0].upper() +
                                 mo.group(0)[1:].lower(),
                      s)

def parseonemovie(text):
    movie = {}
    try:
        m = parsemovie(text)
        movie['genre']        = m.group(1).strip()
        if not movie['genre']:
            movie['genre'] = '--none--'
        if movie['genre'].count('none'):
            movie['genre'] = '--none--'
        movie['genre'] = getChoiceFromRaw(Movie.GENRE_CHOICES,movie['genre'])

        movie['is_silent'] = False
        if movie['genre'] is 'z':
            movie['is_silent'] = True
        
        timeofshowing         = int(m.group(3).strip())
        
        
        movie['path_tcmdb']   = m.group(5).strip()
        movie['path_imdb']    = ''
        movie['rtitle']       = m.group(6).strip()
        movie['year']         = int(m.group(7).strip())
        movie['runtime']      = m.group(9).strip()
        movie['tv_rating']    = m.group(10).strip()
        if not movie['tv_rating']:
            movie['tv_rating'] = '--none--'
        movie['tv_rating']    = getChoiceFromRaw(Movie.TV_RATING_CHOICES,movie['tv_rating'])
                                                   
        movie['description']  = "no description found yet"
        movie['is_color']     = False



        if movie['rtitle'].count(','):
            if movie['rtitle'].endswith(', The'):
                movie['title'] = "The {0}".format(movie['rtitle'][:-5])
            else:
                if movie['rtitle'].endswith(', A'):
                    movie['title'] = "A {0}".format(movie['rtitle'][:-3])
                else:
                    if movie['rtitle'].endswith(', An'):
                        movie['title'] = "An {0}".format(movie['rtitle'][:-4])
                    else:
                        movie['title'] = movie['rtitle']
                                                    
        else:
            if movie['rtitle'].startswith('The '):
                movie['title']  = movie['rtitle']
                movie['rtitle'] = "{0}, The".format(movie['title'][4:])
            else:
                movie['title'] = movie['rtitle']

        if movie['title'].isupper():
            movie['title'] = titlecase(movie['title'])

        if(movie['runtime'].endswith("min")):
            lind = len(movie['runtime'])-4
            movie['runtime']  = movie['runtime'][:lind]

        if(movie['runtime'].startswith("C-")):
            movie['is_color']  = True
            movie['runtime']   = movie['runtime'][2:]

        movie['runtime'] = int(movie['runtime'])
        
        try:
            m2 = parsemovie2(text)
            movie['description'] = m2.group(1).strip()
        except:
            pass

        try:
            m3 = parsemovie3(text)
            movie['cast']        = m3.group(1).strip('.\n')
        except:
            pass

        try:
            m4 = parsemovie4(text)
            movie['director']    = m4.group(1).strip('.\n')
        except:
            pass


            
    except:
        pass

    return (timeofshowing,movie)


def tcmschedulej(request):
    sdate   = request.GET['sdate']
    stime   = time.strptime(sdate,sfmt)
    bdate   = datetime.datetime(stime.tm_year,stime.tm_mon,stime.tm_mday)
    tdelta  = datetime.timedelta(seconds=(4*3600))
    rtext   = ''
    d1      = bdate + tdelta
    d2      = bdate + 7*tdelta
    tschedule  = []
    
    for showing in Showing.objects.filter(datetime__gte=d1).filter(datetime__lte=d2).order_by('datetime'):
        tschedule.append({'datetime': showing.getJSTimestamp(), 'movie'  :showing.movie.toJSONObj()})

    try:
        io = StringIO()
        simplejson.dump(tschedule, io)
        responsetext = io.getvalue()
    
    except Exception as inst:
        responsetext = "error parsing group name from title: ", inst

    return HttpResponse(responsetext)
    #t = loader.get_template("tcmschedulej.html")
    #return HttpResponse(t.render(RequestContext(request,{'tschedule':tschedule,'stime':stime,'d1':d1,'d2':d2})))

def tcmparseshowings(stext):
        msegs     = stext.split("<hr/>")
        showings  = []
        for mseg in msegs:
            try:
                (timeofshowing,parsedm) = parseonemovie(mseg)
                if parsedm['title']:
                    showing_datetime = datetime.datetime.fromtimestamp(timeofshowing)
                    
                    try:
                        #query db to see if movie already exists
                        movieobj    = Movie.objects.get(path_tcmdb=parsedm['path_tcmdb'])
                        movie_added = False
                        
                    except:
                        #if movie not in db, create new object and save
                        movieobj = Movie()
                        movieobj.title        = parsedm['title']
                        movieobj.rtitle       = parsedm['rtitle']
                        movieobj.path_imdb    = parsedm['path_imdb']
                        movieobj.path_tcmdb   = parsedm['path_tcmdb']
                        movieobj.year         = parsedm['year']
                        movieobj.runtime      = parsedm['runtime']
                        movieobj.tv_rating    = parsedm['tv_rating']
                        movieobj.is_color     = parsedm['is_color']
                        movieobj.is_silent    = parsedm['is_silent']
                        movieobj.genre        = parsedm['genre']
                        movieobj.description  = parsedm['description']
                        movieobj.cast         = parsedm['cast']
                        movieobj.director     = parsedm['director']
                        movieobj.save()
                        movie_added           = True
                    try:
                        showingobj  = Showing.objects.get(datetime=showing_datetime)
                        showing_added       = False
                    except:
                        showingobj          = Showing()
                        showingobj.datetime = showing_datetime
                        showingobj.movie    = movieobj
                        showingobj.save()
                        showing_added       = True
   
                    showings.append({'datetime': showingobj.getJSTimestamp(),
                                   'movie'  :  movieobj.toJSONObj(),
                                   'showing_added' : showing_added,
                                   'movie_added'   : movie_added})
            except:
                pass
        return showings
                

