from django.conf.urls.defaults import *

urlpatterns = patterns('',
	(r'^tcmschedule/',  'djangoplex.apps.movies.views.tcmschedule'),
	(r'^tcmschedulej/', 'djangoplex.apps.movies.views.tcmschedulej'),
	(r'^tcmparse',      'djangoplex.apps.movies.views.tcmparse'),
	(r'^tcmparseday',   'djangoplex.apps.movies.views.tcmparseday'),
	(r'^tcmdownload',   'djangoplex.apps.movies.views.tcmdownlaod'),
)