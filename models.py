from django.db import models
from django.contrib import admin
import time
import datetime

# Create your models here.

    
class Movie(models.Model):
    
    GENRE_CHOICES = (
        (u'A', u'adventure'),
        (u'C', u'comedy'),
        (u'c', u'crime'),
        (u'd', u'documentary'),
        (u'D', u'drama'),
        (u'E', u'epic'),
        (u'H', u'horror'),
        (u'M', u'musical'),
        (u'R', u'romance'),
        (u's', u'short'),
        (u'z', u'silent'),
        (u'S', u'suspense'),
        (u'W', u'war'),
        (u'w', u'western'),
        (u'U', u'--none--'),
    )
    TV_RATING_CHOICES = (
        (u'G', u'TV-G'),
        (u'PG', u'TV-PG'),
        (u'14', u'TV-14'),
        (u'MA', u'TV-MA'),
        (u'U', u'--none--'),
    )
    title       = models.CharField(max_length=150)
    rtitle      = models.CharField(max_length=150)
    year        = models.PositiveSmallIntegerField()
    runtime     = models.PositiveSmallIntegerField()
    tv_rating   = models.CharField(max_length=5,choices=TV_RATING_CHOICES)
    is_color    = models.BooleanField(default=False)
    is_silent   = models.BooleanField(default=False)
    path_tcmdb  = models.CharField(max_length=144,unique=True)
    path_imdb   = models.CharField(max_length=102,blank=True)
    genre       = models.CharField(max_length=1,choices=GENRE_CHOICES)
    description = models.TextField(blank=True)
    cast        = models.CharField(max_length=250,blank=True)
    director    = models.CharField(max_length=100,blank=True)
    def __unicode__(self):
        return "{0} ({1})".format(self.title,self.year)
    

    def toJSONObj(self):
        return {'title':self.title,'rtitle':self.rtitle,'year':self.year,
                'runtime':self.runtime,'tv_rating':self.tv_rating,
                'is_color':self.is_color,'is_silent':self.is_silent,'path_tcmdb':self.path_tcmdb,
                'path_imdb':self.path_imdb,'genre':self.genre,'cast':self.cast,
                'director':self.director,'description':self.description}
    
                
        
    class Meta:
        ordering = ['rtitle','-year']

def getChoiceFromRaw(choices,choice):
        for item in choices:
            if choice in item:
                return item[0]
        return 'Q ',choice
    
class Showing(models.Model):
    movie      = models.ForeignKey(Movie)
    datetime   = models.DateTimeField(primary_key=True)
    def __unicode__(self):
        return "{0} -- {1}".format(self.datetime.__str__(),self.movie.title)
    def getJSTimestamp(self):
        return time.mktime(self.datetime.timetuple())*1000
    def toJSONObj(self):
        return {'movie':movie.id, 'datetime': self.getJSTimestamp()}
    class Meta:
        ordering = ['-datetime']
    
class WebPage(models.Model):
    url        = models.URLField()
    datetime   = models.DateTimeField(auto_now_add="True")
    text       = models.TextField()
    def __unicode__(self):
        return self.url

admin.site.register(WebPage)
admin.site.register(Movie)
admin.site.register(Showing)